package Entity;

public class LoggedUser {
  String name;
  int id;

  public LoggedUser(String name, int id) {
    this.name = name;
    this.id = id;
  }

  public LoggedUser() {

  }

  public String getName() {

    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "LoggedUser{" +
        "name='" + name + '\'' +
        ", id=" + id +
        '}';
  }
}


