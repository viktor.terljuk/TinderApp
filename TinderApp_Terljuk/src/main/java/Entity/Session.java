package Entity;

import java.util.ArrayList;
import java.util.List;

public class Session {
    String token;
    User user;
    List<User> likedUsers = new ArrayList<>();

    public Session(String token, User user, List<User> likedUsers) {
        this.token = token;
        this.user = user;
        this.likedUsers = likedUsers;
    }

    public Session() {

    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getLikedUsers() {
        return likedUsers;
    }

    public void setLikedUsers(List<User> likedUsers) {
        this.likedUsers = likedUsers;
    }

    @Override
    public String toString() {
        return "Session{" +
                "token='" + token + '\'' +
                ", user=" + user +
                ", likedUsers=" + likedUsers +
                '}';
    }
}
