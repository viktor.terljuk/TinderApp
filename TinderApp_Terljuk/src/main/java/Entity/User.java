package Entity;

public class User {
  int id;
  String name;
  int age;
  String gender;
  String url;
  boolean liked;
  String username;
  String parol;
  String userRole;

  public String getUserRole() {
    return userRole;
  }

  public void setUserRole(String userRole) {
    this.userRole = userRole;
  }

  public User(String userRole) {
    this.userRole = userRole;
  }

  public User(int id, String name, int age, String gender, String url, boolean liked, String username, String parol) {
    this.id = id;
    this.name = name;
    this.age = age;
    this.gender = gender;
    this.url = url;
    this.liked = liked;
    this.username = username;
    this.parol = parol;
  }

  public User() {

  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public boolean isLiked() {
    return liked;
  }

  public void setLiked(boolean liked) {
    this.liked = liked;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getParol() {
    return parol;
  }

  public void setParol(String parol) {
    this.parol = parol;
  }

  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", age=" + age +
        ", gender='" + gender + '\'' +
        ", url='" + url + '\'' +
        ", liked=" + liked +
        ", username='" + username + '\'' +
        ", parol='" + parol + '\'' +
        ", userRole='" + userRole + '\'' +
        '}';
  }
}
