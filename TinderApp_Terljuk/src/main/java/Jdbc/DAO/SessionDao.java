package Jdbc.DAO;

import Entity.Session;

import java.util.List;

public interface SessionDao {
    List<Session> getAll();

    void delete(String token);

    void add(Session session);
}
