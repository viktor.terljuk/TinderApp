package Jdbc;

import Entity.User;
import Mapper.UserMapper;
import Service.ConnectToDataBase;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcLikedUsersDao {

    ConnectToDataBase connectToDataBase = new ConnectToDataBase();

    public void save(User user) throws SQLException {
        Connection connection = connectToDataBase.getConnection();

        String sql = "INSERT INTO terljuk_tinder_liked (Name, Gender, Age, Url) VALUE (?, ?, ?, ?);";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, user.getName());
        preparedStatement.setString(2, user.getGender());
        preparedStatement.setInt(3, user.getAge());
        preparedStatement.setString(4, user.getUrl());
        preparedStatement.executeQuery();
    }

    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        Connection connection = connectToDataBase.getConnection();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM terljuk_tinder_liked");
            UserMapper userMapper = new UserMapper();

            while (resultSet.next()) {
                User user = userMapper.mapRow(resultSet);
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }
}
