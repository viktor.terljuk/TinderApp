package Jdbc;

import Entity.LoggedUser;
import Service.ConnectToDataBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JdbcLoggedUserDao {

  ConnectToDataBase connectToDataBase = new ConnectToDataBase();


  public void saveCurrentUser (LoggedUser loggedUser) throws SQLException {
    Connection connection = connectToDataBase.getConnection();

    String sql = "INSERT INTO terljuk_tinder_liked (userId) VALUE (?);";
    PreparedStatement preparedStatement = connection.prepareStatement(sql);
    preparedStatement.setInt(1, loggedUser.getId());
    preparedStatement.executeUpdate();
  }
}
