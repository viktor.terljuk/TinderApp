package Jdbc;

import Entity.User;
import Mapper.UserMapper;
import Service.ConnectToDataBase;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcUserDao {

  ConnectToDataBase connectToDataBase = new ConnectToDataBase();

  public void save(User user) throws SQLException {
    Connection connection = connectToDataBase.getConnection();

    String sql = "INSERT INTO terljuk_tinder (Name, Gender, Age, Url, Username, Parol, UserRole) VALUE (?, ?, ?, ?, ?, ?, ?);";
    PreparedStatement preparedStatement = connection.prepareStatement(sql);
    preparedStatement.setString(1, user.getName());
    preparedStatement.setString(2, user.getGender());
    preparedStatement.setInt(3, user.getAge());
    preparedStatement.setString(4, user.getUrl());
    preparedStatement.setString(5, user.getUsername());
    preparedStatement.setString(6, user.getParol());
    preparedStatement.setString(7, user.getUserRole());
    preparedStatement.executeUpdate();
  }

 /* public void saveLiked(User likedUser) throws SQLException {
    Connection connection = connectToDataBase.getConnection();

    String sql = "INSERT INTO terjuk_tinder_liked (userId, likedId) VALUES (?, ?)";
    PreparedStatement preparedStatement = connection.prepareStatement(sql);
    preparedStatement.setInt();
    preparedStatement.executeUpdate();
  }*/

  public List<User> getAll() {
    List<User> users = new ArrayList<>();
    Connection connection = connectToDataBase.getConnection();

    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM terljuk_tinder");
      UserMapper userMapper = new UserMapper();

      while (resultSet.next()) {
        User user = userMapper.mapRow(resultSet);
        users.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return users;
  }

  public List<User> getAllLiked() {
    List<User> users = new ArrayList<>();
    Connection connection = connectToDataBase.getConnection();

    try {
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM terjuk_tinder_liked");
      UserMapper userMapper = new UserMapper();

      while (resultSet.next()) {
        User user = userMapper.mapRow(resultSet);

        if (!users.contains(user)) {
          users.add(user);
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return users;
  }

 /* private Connection getConnection() {

    String url = "jdbc:mysql://danit.cukm9c6zpjo8.us-west-2.rds.amazonaws.com:3306/fs3";
    String user = "fs3_user";
    String password = "bostoN";
    Connection connection = null;

    try {
      connection = DriverManager.getConnection(url, user, password);
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return connection;
  }*/
}
