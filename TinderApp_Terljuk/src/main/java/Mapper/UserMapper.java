package Mapper;

import Entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper {
  public User mapRow(ResultSet resultSet) throws SQLException {
    User user = new User();
    user.setId(resultSet.getInt("id"));
    user.setName(resultSet.getString("name"));
    user.setGender(resultSet.getString("gender"));
    user.setAge(resultSet.getInt("age"));
    user.setUrl(resultSet.getString("url"));
    user.setUsername(resultSet.getString("username"));
    user.setParol(resultSet.getString("parol"));
    user.setUserRole(resultSet.getString("userRole"));
    return user;
  }
}
