package Service;

import Entity.Session;

import java.util.List;

public class SessionService {

  SessionStore sessionStore = new SessionStore();

  public SessionService() {

  }

  public List<Session> getAllSessions () {
    return sessionStore.getAll();
  }

  public SessionService(SessionStore sessionStore) {
    this.sessionStore = sessionStore;
  }

  public void setSessionStore(SessionStore sessionStore) {
    this.sessionStore = sessionStore;
  }
}
