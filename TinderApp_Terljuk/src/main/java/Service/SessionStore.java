package Service;

import Entity.Session;

import java.util.ArrayList;
import java.util.List;

public class SessionStore {

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    List<Session> sessions = new ArrayList<>();


    public List<Session> getAll() {
        return sessions;
    }

    public void add(Session session) {
        sessions.add(session);
    }

    public Session get (String token) {
        for (Session session : sessions) {
            if (session.getToken().equals(token)) {
                return session;
            }
        }
        return null;
    }

    public void delete(String token) {
        for (Session session : sessions) {
            if (session.getToken().equals(token)) {
                sessions.remove(session);
            }
        }
    }

}
