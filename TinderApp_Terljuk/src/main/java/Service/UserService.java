package Service;

import Entity.LoggedUser;
import Entity.User;
import Jdbc.JdbcLikedUsersDao;
import Jdbc.JdbcLoggedUserDao;
import Jdbc.JdbcUserDao;

import java.sql.SQLException;
import java.util.List;

public class UserService {
  LoggedUser loggedUser = new LoggedUser();
  private JdbcUserDao userDao = new JdbcUserDao();
  private JdbcLikedUsersDao likedUsersDao = new JdbcLikedUsersDao();
  SessionStore sessionStore = new SessionStore();
  JdbcLoggedUserDao jdbcLoggedUserDao = new JdbcLoggedUserDao();

  public void setUserDao(JdbcUserDao userDao) {
    this.userDao = userDao;
  }

  public List<User> getAllUsers() {
    return userDao.getAll();
  }

  public List<User> getAllLikedUsers() {
    return userDao.getAllLiked();
  }
  public User auth(String username, String password) {
    List<User> users = userDao.getAll();

    for (User user : users) {
      if (user.getUsername().equals(username) && user.getParol().equals(password)) {
        loggedUser.setId(user.getId());
        loggedUser.setName(user.getName());
        try {
          jdbcLoggedUserDao.saveCurrentUser(loggedUser);
        } catch (SQLException e) {
          e.printStackTrace();
        }
        return user;
      }
    }
    return null;
  }
}