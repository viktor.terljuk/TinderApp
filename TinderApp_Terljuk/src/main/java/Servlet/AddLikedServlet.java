package Servlet;

import Entity.User;
import Jdbc.DAO.UserDao;
import Jdbc.JdbcLikedUsersDao;
import Service.UserService;
import Templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddLikedServlet extends HttpServlet {
    UserService userService = new UserService();
    JdbcLikedUsersDao likedUsersDao = new JdbcLikedUsersDao();
    UserDao userDao = new UserDao();
    // JdbcUserDao userDao = new JdbcUserDao();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().write(getPage());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        int age = Integer.parseInt(req.getParameter("age"));
        String gender = req.getParameter("gender");
        String url = req.getParameter("url");

        User user = new User();
        user.setName(name);
        user.setAge(age);
        user.setGender(gender);
        user.setUrl(url);

        try {
            addUser(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        resp.sendRedirect("/liked");
    }


    private void addUser(User user) throws SQLException {
        likedUsersDao.save(user);
    }

    private String getPage() {
        List<User> likedUsers = userService.getAllLikedUsers();
        PageGenerator pageGenerator = PageGenerator.instance();
        Map<String, Object> likedUsersMap = new HashMap<>();
        likedUsersMap.put("users", likedUsers);
        String page = pageGenerator.getPage("Liked.ftl", likedUsersMap);
        return page;
    }
}
