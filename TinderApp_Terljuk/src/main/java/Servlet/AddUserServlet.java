package Servlet;

import Entity.User;
import Jdbc.JdbcUserDao;
import Service.UserService;
import Templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddUserServlet extends HttpServlet {
    UserService userService = new UserService();
    JdbcUserDao userDao = new JdbcUserDao();

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    List<User> users = userService.getAllUsers();
    PageGenerator pageGenerator = PageGenerator.instance();
    Map<String, Object> userMap = new HashMap<>();
    userMap.put("users", users);
    String page = pageGenerator.getPage("AddUser.ftl", userMap);
    resp.getWriter().write(page);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String name = req.getParameter("Name");
    String gender = req.getParameter("Gender");
    int age = Integer.parseInt(req.getParameter("Age"));
    String url = req.getParameter("Url");
    boolean liked = Boolean.parseBoolean(req.getParameter("Liked"));
    String username = req.getParameter("username");
    String parol = req.getParameter("parol");

    User user = new User();
    user.setName(name);
    user.setAge(age);
    user.setGender(gender);
    user.setUrl(url);
    user.setLiked(liked);
    user.setUsername(username);
    user.setParol(parol);

    try {
      addUser(user);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    resp.sendRedirect("/user/add");
  }


  private void addUser(User user) throws SQLException {
    userDao.save(user);
  }
}
