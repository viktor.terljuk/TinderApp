package Servlet;

import Entity.User;
import Jdbc.JdbcUserDao;
import Service.UserService;
import Templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class LikedServlet extends HttpServlet {
    UserService userService = new UserService();
    JdbcUserDao jdbcUserDao = new JdbcUserDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().write(getPage());
    }


    public String getPage() {
        List<User> users = userService.getAllLikedUsers();
        PageGenerator pageGenerator = PageGenerator.instance();
        HashMap<String, Object> map = new HashMap<>();
        map.put("users", users);
        String page = pageGenerator.getPage("LikedUsers.ftl", map);
        return page;
    }

    private User getUser(int index) {
        List<User> users = jdbcUserDao.getAll();

        for (User user : users) {

            if (user.getId() == index) {
                return user;
            }
        }
        return null;
    }
}
