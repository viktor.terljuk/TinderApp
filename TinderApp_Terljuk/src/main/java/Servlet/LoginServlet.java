package Servlet;

import Entity.Session;
import Entity.User;
import Jdbc.JdbcLoggedUserDao;
import Service.SessionStore;
import Service.UserService;
import Templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class LoginServlet extends HttpServlet {

    UserService userService = new UserService();
    SessionStore sessionStore = new SessionStore();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().write(getPage());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        User user = userService.auth(username, password);
        if (user != null) {
            String token = UUID.randomUUID().toString();
            Cookie cookie = new Cookie("user-token", token);
            Session session;
            sessionStore.add(session = new Session());
            session.setToken(token);
            session.setUser(user);
            resp.addCookie(cookie);
        }
        resp.sendRedirect("/game");
        if (user == null) {
            resp.sendRedirect("/login");
        }
    }

    public String getPage() {
        PageGenerator pageGenerator = PageGenerator.instance();
        HashMap<String, Object> map = new HashMap<>();
        String page = pageGenerator.getPage("LoginPage.ftl", map);
        return page;
    }


}
