package Servlet;

import Templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainPageServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    PageGenerator pageGenerator = PageGenerator.instance();
    HashMap<String, Object> map = new HashMap<>();
    List<String> users = new ArrayList<>();
    users.add("The Stig");
    map.put("users", users);
    String page = pageGenerator.getPage("StaticPage.ftl", map);
    resp.getWriter().write(page);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

  }
}
