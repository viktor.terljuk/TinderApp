package Servlet;

import Entity.User;
import Jdbc.JdbcUserDao;
import Templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class MessageServlet extends HttpServlet {
    JdbcUserDao userDao = new JdbcUserDao();
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    int id = Integer.parseInt(req.getParameter("userId"));
    User user = getUser(id);
    PageGenerator pageGenerator = PageGenerator.instance();
    HashMap<String, Object> map = new HashMap<>();
    map.put("user", user);
    String page = pageGenerator.getPage("Message.ftl", map);
    resp.getWriter().write(page);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.getWriter().write(getPage());
  }

  public String getPage() {
    List<User> userToMassage = userDao.getAll();
    PageGenerator pageGenerator = PageGenerator.instance();
    HashMap<String, Object> map = new HashMap<>();
    map.put("user", userToMassage);
    String page = pageGenerator.getPage("Message.ftl", map);
    return page;
  }

  private User getUser(int id) {
    List<User> users = userDao.getAll();

    for (User user : users) {

      if (user.getId() == id) {
        return user;
      }
    }
    return null;
  }
}
