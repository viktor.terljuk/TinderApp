package Servlet;

import Entity.LikedList;
import Jdbc.DAO.UserDao;
import Entity.User;
import Jdbc.JdbcLikedUsersDao;
import Jdbc.JdbcUserDao;
import Templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RandomStaticPageServlet extends HttpServlet {
    //UserDao userDao = new UserDao();
    JdbcUserDao userDao = new JdbcUserDao();
    JdbcLikedUsersDao likedUsersDao = new JdbcLikedUsersDao();
    List<User> users = userDao.getAll();
    public List<User> liked = new ArrayList<>();
    LikedList lkedList = new LikedList();



  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
     resp.getWriter().write(getPage());
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    int id = Integer.parseInt(req.getParameter("id"));
      User user = getUser(id);
      User likedUser = getUser(id);
      liked.add(likedUser);
      lkedList.saveLikedToArray(likedUser);
     /* if (!liked.contains(user)) {
          liked.add(user);
      }*//*
      for (User user1 : liked) {
          System.out.println(user1);
      }*/
      resp.getWriter().write(getPage());
  }

  private String getPage() {
    PageGenerator newPageGenerator = PageGenerator.instance();
    HashMap<String, Object> mapNew = new HashMap<>();
    int index = (int) (Math.random()*userDao.getAll().size());
    mapNew.put("user", users.get(index));
    String page = newPageGenerator.getPage("StatikPageChangingPhotos.ftl", mapNew);
    return page;
  }

    private User getUser(int index) {
      List<User> users = userDao.getAll();

        for (User user : users) {

            if (user.getId() == index) {
                return user;
            }
        }
        return null;
  }

}
