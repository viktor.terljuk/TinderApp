package Servlet;

import Entity.User;
import Jdbc.JdbcUserDao;
import Service.UserService;
import Templater.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class UserServlet extends HttpServlet {
  UserService userService = new UserService();
  JdbcUserDao userDao = new JdbcUserDao();


  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    List<User> users = userService.getAllUsers();
    PageGenerator pageGenerator = PageGenerator.instance();
    HashMap<String, Object> map = new HashMap<>();
    map.put("users", users);
    String page = pageGenerator.getPage("Users.ftl", map);
    resp.getWriter().write(page);
  }
}
