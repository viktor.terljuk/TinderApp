package Servlet;

import Entity.Session;
import Entity.User;
import Entity.UserRole;
import Service.SessionService;
import Service.SessionStore;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Filter;
import java.util.logging.LogRecord;

public class WebFilter implements javax.servlet.Filter {

  SessionService sessionService;


  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response = (HttpServletResponse) resp;
    boolean loggedIn = false;

    if (request.getRequestURI().equals("/login")) {
      chain.doFilter(req, resp);
    } else {
      if (loggedIn) {
        response.sendRedirect("/login");
      } else {
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {

          for (Cookie cookie : request.getCookies()) {

            if ("user-token".equals(cookie.getName())) {
              chain.doFilter(req, resp);
              break;
            } else {
              cookie.setMaxAge(0);
              response.addCookie(cookie);
              response.sendRedirect("/login");
            }
          }
        } else {
          response.sendRedirect("/login");
          loggedIn = true;
        }
      }

    }
  }

  @Override
  public void destroy() {

  }

  public void setSessionService(SessionService sessionService) {
    this.sessionService = sessionService;
  }
}
