package Starter;

import Service.SessionService;
import Service.SessionStore;
import Servlet.*;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.http.HttpServlet;

import java.net.*;
import java.util.EnumSet;

import static javax.servlet.DispatcherType.REQUEST;

public class Starter extends HttpServlet{
  public SessionService sessionService = new SessionService();

  public static void main(String[] args) throws Exception {
    org.eclipse.jetty.server.Server server = new org.eclipse.jetty.server.Server(8080);
    SessionService sessionService = new SessionService();
    ServletContextHandler handler = new ServletContextHandler();

    ServletHolder mainPageServletHolder = new ServletHolder(new MainPageServlet());
    handler.addServlet(mainPageServletHolder, "/main");

    ServletHolder addUserServletHolder = new ServletHolder(new AddUserServlet());
    handler.addServlet(addUserServletHolder, "/user/add");

    ServletHolder userServletHolder = new ServletHolder(new UserServlet()) ;
    handler.addServlet(userServletHolder, "/user");

    ServletHolder userDaoHolder = new ServletHolder(new RandomStaticPageServlet());
    handler.addServlet(userDaoHolder, "/game");

    ServletHolder likedServletHolder = new ServletHolder(new LikedServlet()) ;
    handler.addServlet(likedServletHolder, "/liked");

    ServletHolder massageServletHolder = new ServletHolder(new MessageServlet());
    handler.addServlet(massageServletHolder, "/message");

    ServletHolder loginServletHolder = new ServletHolder(new LoginServlet());
    handler.addServlet(loginServletHolder, "/login");

    FilterHolder filterHolder = new FilterHolder(new WebFilter());
    handler.addFilter(filterHolder, "/*", EnumSet.of(DispatcherType.REQUEST));

    WebFilter webFilter = new WebFilter();
    webFilter.setSessionService(sessionService);


    /*ServletHolder addLikedServletHolder = new ServletHolder(new AddLikedServlet()) ;
    handler.addServlet(userServletHolder, "/liked");*/
    server.setHandler(handler);
    server.start();
    server.join();
  }
}
