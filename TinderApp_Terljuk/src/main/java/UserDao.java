package Jdbc.DAO;

import Entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserDao {

    private List<User> users = new ArrayList<>();

    public UserDao() {
        users.add(new User(1, "Jeremy Clarkson", 57, "male", "https://static2.hypable.com/wp-content/uploads/2015/03/top-gear-jeremy-clarkson-fired-wide-3.jpg", false, "jeremy", "123"));
        users.add(new User(2, "Richard Hammond", 48, "male", "http://imagesvc.timeincapp.com/v3/foundry/image/?q=70&w=1440&url=https%3A%2F%2Ftimedotcom.files.wordpress.com%2F2017%2F06%2Fhammond-wheelchair.jpg%3Fquality%3D85", false, "richard", "123"));
        users.add(new User(3, "James May", 55, "male", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSa79qUAhP0UgrvRjWNPLM1OEWimdfcyiDgq-Q3WOVxc3K1YlCi", false, "james", "123"));
    }

    public void save(User user) {
        user.setId(users.size() + 1);
        users.add(user);
    }

    public List<User> getAll() {
        return new ArrayList<>(users);
    }
}
