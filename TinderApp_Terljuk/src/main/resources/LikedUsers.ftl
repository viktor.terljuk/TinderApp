<!DOCTYPE html>
<html>
<head>
    <title>Liked</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <style>
        .user {
            background-color: #d1273f;
            align-content: end;
            color: #fecb30;
            width: 250px;
            height: 50px;
            border: solid white 2px;
            }
        body {
            background-color: #2d203a;
        }
        .table {
            justify-content: center;
            text-align-center;

        }
        .name {
            color: #7c2b40;
            border: #2d203a;
            border-top-style: ;
        }
        #line {
            width: 550px;
        }
        #link {
            color: #fecb30;
        }

    </style>
</head>
<body>
<#list users as user>
<div class="row justify-content-md-center">
    <div class="thumbnail w-60">
    <img class="img-thumbnail" src="${user.url}" width="825">
    <table class="table">
        <tr>
             <th class="name">${user.name}</th>
        </tr>
        <tr id="line">
             <td class="user">${user.age}</td>
             <td class="user">${user.gender}</td>
             <td class="user">
                 <form action="/liked" method="post">
                     <input  class="id" type='hidden' name='id' value='${user.id}'>
                 <a id="link" href="http://localhost:8080/message?userId=${user.id}">Message</a>
                 </form>
             </td>
        <ul>
        </tr>
            </table>
    </div>
</div>
</#list>
</body>
</html>